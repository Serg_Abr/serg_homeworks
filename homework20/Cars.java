public class Cars {
    private final String number;
    private final String name;
    private final String color;
    private final int mileage;
    private final int price;

    public Cars(String number, String name, String color, int mileage, int price) {
        this.number = number;
        this.name = name;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }
}
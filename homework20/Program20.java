import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Program20 {
    public static void main(String[] args) throws IOException {
        System.out.println("Номера всех автомобилей, имеющих черный цвет или нулевой пробег:");
        getNumbersColorBlackAndZeroMillage();
        System.out.println();
        System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.: " + getCountCars());
        System.out.println();
        System.out.println("Цвет автомобиля с минимальной стоимостью: " + getColorForMinPrice());
        System.out.println();
        System.out.println("Средняя стоимость Toyota: " + ( getSumPriceName() / getCountToyota()));
    }

    public static List<Cars> getListCars() throws IOException {
        List<String> list = Files.readAllLines(Path.of("src/input.txt"));
        return list.stream()
                .map(line -> line.split("\\|"))
                .map(field -> new Cars(field[0], field[1], field[2],
                        Integer.parseInt(field[3]), Integer.parseInt(field[4]))).collect(Collectors.toList());
    }

    // метод поиска автомобильных номеров автомобилей черного цвета или с нулевым пробегом
    public static void getNumbersColorBlackAndZeroMillage() throws IOException {
        getListCars().stream().filter(car -> car.getColor().equals("Black") || car.getMileage() == 0)
                .forEach(cars -> System.out.println(" - " + cars.getNumber()));
    }

    // метод поиска количества уникальных названий автомобилей в указанном ценовом диапазоне
    public static long getCountCars() throws IOException {
        return getListCars().stream().filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .map(car -> car.getName())
                .distinct()
                .count();

    }
    // метод поиска цвета автомобиля с минимальной ценой
    public static String getColorForMinPrice() throws IOException {
        return getListCars().stream().min(Comparator.comparingInt(Cars::getPrice)).map(Cars::getColor).get();
    }

    // метод поиска суммы автомобилей Toyota
    public static int getSumPriceName() throws IOException {
        int sum = getListCars().stream().filter(car -> car.getName().equals("Toyota")).mapToInt(Cars::getPrice).sum();
        return sum;
    }

    // метод поиска количества автомобилей Toyota
    public static long getCountToyota() throws IOException {
        return getListCars().stream().filter(car -> car.getName().equals("Toyota"))
                .count();
    }
}
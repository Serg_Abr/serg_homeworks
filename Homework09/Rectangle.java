class Rectangle extends Figura{
	protected int a;
	private int b;

	public Rectangle(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public double getPerimetr() {return a * 2 + b * 2;}

}
class Program09 {
	public static void main(String[] args) {
		Figura figura = new Figura();
		Rectangle rectangle = new Rectangle(10, 15);
		Square square = new Square(5);
		Ellipse ellipse = new Ellipse(5.0, 4.0);
		Circle circle = new Circle(5);

		System.out.println("�������� ������ - " + figura.getPerimetr());
		System.out.println("�������� �������������� - " + rectangle.getPerimetr());
		System.out.println("�������� �������� - " + square.getPerimetr());
		System.out.println("�������� ����� - " + ellipse.getPerimetr());
		System.out.println("�������� ����� - " + circle.getPerimetr());
	}
}
class Circle extends Ellipse {
	public Circle (double r1) {
		super(r1, r1);
	}

	public double getPerimetr() {return 2 * Math.PI * r1;}
}
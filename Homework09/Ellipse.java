class Ellipse extends Figura{
	protected double r1;
	private double r2;

	public Ellipse(double r1, double r2) {
		this.r1 = r1;
		this.r2 = r2;
	}

	public double getPerimetr() {return 2 * Math.PI * Math.pow(((r1 * r1 + r2 * r2) / 2), 0.5);}

}
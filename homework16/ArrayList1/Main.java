package ArrayList1;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(45);
        numbers.add(78);
        numbers.add(10);
        numbers.add(17);
        numbers.add(89);
        numbers.add(16);

        numbers.removeAt(3);

        for(int i = 0; i < numbers.getSize(); i++){
            System.out.println(numbers.getNum(i));
        }
    }
}
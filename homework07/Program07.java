import java.util.Scanner;

class Program07 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		long [] arrNumber = new long [201]; // задаем массив для хранения количества вхождения каждого числа
		int next = sc.nextInt();		  // считываем первое число с консоли	
		int mincount = 0;	// задаем переменную, для хранения минимального числа вхождения в цикл
		long min = 0;
	
		// запускаем цикл, до тех пор пока не появится точка остановки считывания с консоли -1
		while (next != -1) {
			arrNumber[next + 100]++; // увеличиваем элемент равный числу + 100
			next = sc.nextInt();  // считываем следующее число
		}

		// находим первое число которое больше 0 и присваеваем его индекс минимальному числу вхождений
		for (int i = 0; i < arrNumber.length; i++) {
			if (arrNumber[i] != 0) {
				mincount = i;
				min = arrNumber[i];
				break;
			}
		}

		// запускаем цикл для нахождения минимального числа  не равного 0
		for (int i = 0; i < arrNumber.length; i++) {
			if ((arrNumber[i] != 0) & (arrNumber[i] < min)) {
				min = arrNumber[i];
				mincount = i;
			}
		}
		
		System.out.print(mincount - 100); // выводим число, которое всчречается минимальное число раз
	}
}
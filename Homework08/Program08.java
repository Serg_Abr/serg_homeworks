import java.util.Arrays;

class Program08 {
	public static void main(String[] args) {
		
		Human man1 = new Human("Vasja", 77.6);
	    Human man2 = new Human("Gena", 67.5);
	    Human man3 = new Human("Ivan", 82.1);
	    Human man4 = new Human("Grisha", 67.6);
	    Human man5 = new Human("Nikita", 92.9);
	    Human man6 = new Human("Gleb", 81.6);
	    Human man7 = new Human("Kolja", 79.2);
	    Human man8 = new Human("Artem", 90.1);
	    Human man9 = new Human("Roma", 107.6);
	    Human man10 = new Human("Sasha", 88.8);

	    Human [] mans = {man1, man2, man3, man4, man5, man6, man7, man8, man9, man10};
	    Arrays.sort(mans);

	    for(Human p: mans) {
	        System.out.println("Name - " + p.getName() + ". Weight - " + p.getWeight());
	    }
	}
}
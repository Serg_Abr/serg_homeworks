class Human implements Comparable<Human> {
private String name;
private double weight;

Human(String name, double weight){
    this.name = name; this.weight = weight;

}

public String getName() {
    return name;
}

public void setName(String name) {
    this.name = name;
}

public double getWeight() {
    return weight;
}

public void setWeight(int weight) {
    if (weight > 0.0 || weight < 150.0) {
        weight = 0;
    }
    this.weight = weight;
}


@Override
public int compareTo(Human other) {
    if(this.getWeight() > other.getWeight())
        return 1;
    else if (this.getWeight() == other.getWeight())
        return 0 ;
    return -1 ;
}
}
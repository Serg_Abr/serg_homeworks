public class Logger {
    //статическое (глобальное) поле, которое хранит единственный экземпляр класса
    private static final Logger instance;
    // определяем переменную для работы
    private  String message;
    //статический инициализатор, который создает единственный объект класса
    static {
        instance = new Logger();
    }
    //статический метод для получения единственного экземпляра класса
    public static Logger getInstance() {
        return instance;
    }
    // приватный конструктор класса Logger, запрещающий создание объектов
    private Logger(){
        this.message = "Message";
    }
    // процедура log для вывода сообщения
    public static void log(String message){
        System.out.println(message);
    }
}

package Homework17;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        // String text = "a aa aaa aaaa aaa asa a aaa asa adsa aaaa";
        Scanner scan = new Scanner(System.in);
        String[] words = scan.nextLine().split(" ");

        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            Integer count = map.get(word);
            if (count == null) {
                count = 0;
            }
            map.put(word, count + 1);
        }

        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        for (Map.Entry<String, Integer> entry : entries){
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }
}
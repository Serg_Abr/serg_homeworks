import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("src/users.txt");
        int newAge = -1;

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите ID пользователя: ");
        int findId = scan.nextInt();
        User userId = usersRepository.findById(findId);

        if (userId == null) {
            System.out.println("Данный ID не используется");
        } else {
            System.out.println("Введите новое имя пользователя: ");
            String newName = scan.next();
            userId.setName(newName);
            while (newAge < 18 || newAge > 80) {
                System.out.println("Введите новый возраст пользователя: ");
                newAge = scan.nextInt();
            }
            userId.setAge(newAge);

            usersRepository.update(userId);
        }
    }
}

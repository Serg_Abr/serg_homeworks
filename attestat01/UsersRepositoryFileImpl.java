import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public User findById(int idUser) {
        try {
            FileReader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int currIdUser = Integer.parseInt(parts[0]);
                if (currIdUser == idUser) {
                    return new User(currIdUser, parts[1], Integer.parseInt(parts[2]), Boolean.parseBoolean(parts[3]));
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(User user) {
        try {
            List<String> file = Files.readAllLines(Path.of(fileName));
            List<User> userAll = file.stream()
                    .map(line -> line.split("\\|"))
                    .map(field -> new User(Integer.parseInt(field[0]), field[1], Integer.parseInt(field[2]), Boolean.parseBoolean(field[3]))).collect(Collectors.toList());//.collect(Collectors.toList())
            User finalUser = user;
            userAll.removeIf(nextUser -> nextUser.getIdUser() == finalUser.getIdUser());
            userAll.add(user);
            cleanFile();

            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false));

            for (User value : userAll) {
                user = value;
                bufferedWriter.write(user.getIdUser() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void cleanFile() throws IOException {
        PrintWriter writer = new PrintWriter(fileName);
        writer.print("");
        writer.close();
    }

}
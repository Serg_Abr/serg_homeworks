class Program06 {

/**
функция возвращает интекс вхождения числа num  в массив arr
*/
public static int getDigitalIndex(int [] arr, int num) {
	int digIndex = -1; // индекс искомого числа или -1, если числа нет в массиве
	// запускаем цикл для проверки нахождения числа в массиве
	for (int i = 0; i < arr.length; i++) {
		// проверяем равен ли i-ый элемент числу num
		if (arr[i] == num) {
			digIndex = i; // если равен присваеваем индекс и выходим из цикла
			break;
		}
	}
	return digIndex;
}

/**
процедура сдвига нулей в конец массива
*/
public static void printArr (int [] arr) {
	int lenArr = arr.length;              // определяем длину массива
	// запускаем цикл для проверки элемента массива на 0
	for (int i = 0; i < lenArr; i++) {
		// если элемент массива равен 0 совершаем сдвиг влево
		if (arr[i] == 0) {
			for (int j = i; j < (lenArr - 1); j++) {
				arr[j] = arr[j + 1];
			}
			// присваеваем последнему элементу -1
			arr[lenArr - 1] = -1;
		}
		// проверяем, что новый i-ый элемент не равен 0, если равен, то уменьшаем i на 1
		if (arr[i] == 0) {
			i -= 1;
		}
	}
	// выводим новый массив на консоль, заменяя -1 на 0
	for (int i = 0; i < arr.length; i++){
		if (arr[i] == -1) {
			arr[i] = 0;
		}
		System.out.print(arr[i] + " ");
	}
}


	public static void main(String[] args) {
		
		// задаем массив и переменные для контроля работы функции
		int [] arr1 = {12, 10, 3, 34, 12, 65, 22};
		int a = 65;
		int b = 12;
		int c = 13;
		int digitalIndex1 = getDigitalIndex(arr1, a);
		int digitalIndex2 = getDigitalIndex(arr1, b);
		int digitalIndex3 = getDigitalIndex(arr1, c);
		System.out.println("Index digital a: " + digitalIndex1);
		System.out.println("Index digital b: " + digitalIndex2);
		System.out.println("Index digital c: " + digitalIndex3);
		System.out.println();
		System.out.println();

		// задаем массив для контроля работы процедуры
		int [] arr2 = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
		printArr(arr2);
	}
}
public class Circle extends Figura {
    public Circle(int currX, int currY) {
        super(currX, currY);
        this.goFigur = "Circle";
    }

    @Override
    public void getMove() {
        this.currX = currX;
        this.currY = currY;
    }

    public int getCurrX() {
        return currX;
    }

    public int getCurrY() {
        return currY;
    }

    public String getGoFigur() {
        return goFigur;
    }
}

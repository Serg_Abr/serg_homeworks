public abstract class Figura {
    protected int currX;
    protected int currY;
    protected String goFigur;

    protected Moving moving;

    public Figura(int currX, int currY) {
        this.currX = currX;
        this.currY = currY;
    }

    public abstract void getMove();

    public void setMoving(Moving moving){
        this.moving = moving;
    }

//    public void getGoY(){
//
//    }
//
//    public void getGoFigur(){
//
//    }
}

public class Square extends Figura{
    public Square(int currX, int currY){
        super(currX, currY);
        this.goFigur = "Square";
    }

    @Override
    public void getMove(){
        this.currX = currX;
        this.currY = currY;
    }

    public int getCurrX(){
        return currX;
    }

    public int getCurrY(){
        return currY;
    }

    public String getGoFigur(){
        return goFigur;
    }
}

class Program10 {
    public static void main(String[] args) {
        Square go1 = new Square(10, 10);
        Square go2 = new Square(5, 12);
        Circle go3 = new Circle(15, 14);
        Circle go4 = new Circle(40, 25);
        MovingXY mov = new MovingXY();

        Figura[] moving = {go1, go2, go3, go4};

        for(Figura z: moving) {
            z.currX = mov.getGoX();
            z.currY = mov.getGoY();
        }

        for(Figura w: moving) {
            System.out.println(w.goFigur + " перемещен в координаты: " + w.currX + " " + w.currY);
        }
//        System.out.println(go1.getGoFigur() + " перемещен в координаты: " + go1.getCurrX() + " " + go1.getCurrY());
//        System.out.println(go2.getGoFigur() + " перемещен в координаты: " + go2.getCurrX() + " " + go2.getCurrY());
//        System.out.println(go3.getGoFigur() + " перемещен в координаты: " + go3.getCurrX() + " " + go3.getCurrY());
//        System.out.println(go4.getGoFigur() + " перемещен в координаты: " + go4.getCurrX() + " " + go4.getCurrY());
    }
}

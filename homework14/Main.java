import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int [] array = {23, 46, 255, 456, 57, 348, 111};
        int [] a = Sequence.filter(array, (number) -> (number%2 == 0));

        System.out.println(Arrays.toString(a));

        int [] b = Sequence.filter(array, number ->  {
                int sum = 0;
                while (number != 0) {
                    int lastNum = number % 10;
                    sum += lastNum;
                    number = number / 10;
                }
                return  sum % 2 == 0;
        });

        System.out.print(Arrays.toString(b));
        }
}

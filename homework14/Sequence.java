import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition){
        int[] result = new int[array.length];
        int co =0;
        for(int i = 0; i < array.length; i++){
            if (condition.isOk(array[i]) == true) {
                result[co] = array[i];
                co++;
            }
        }
        return Arrays.copyOf(result, co);
    }
}

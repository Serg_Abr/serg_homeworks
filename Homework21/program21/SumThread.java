package program21;

import static program21.Main.array;
import static program21.Main.sums;


public class SumThread extends Thread{
    private int from;
    private int to;
    private int t;

    public SumThread(int from, int to, int i) {
        this.from = from;
        this.to = to;
        this.t = i;
    }

    @Override
    public void run() {
        for (int j = from; j <= to; j++){
            sums[t] += array[j];
        }
    }
}

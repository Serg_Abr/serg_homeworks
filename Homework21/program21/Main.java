package program21;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int[] array;
    public static int[] sums;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число элементов:");
        int numbersCount = scan.nextInt();
        System.out.println("Введите число потоков:");
        int threadsCount = scan.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int j : array) {
            realSum += j;
        }

        System.out.println("сумма вычисленная в 1 потоке - " + realSum);

        int threadLength;
        if (array.length % threadsCount == 0) threadLength = array.length/threadsCount;
        else threadLength = array.length/threadsCount + 1;
        SumThread[] anotherThread = new SumThread[threadsCount];
        
        for (int i = 0; (i+1)*threadLength < array.length+1; i++){
            anotherThread[i] = new SumThread(i * threadLength, (i+1) * threadLength - 1, i);
        }
        
        int k = 0;
        if (array.length % threadsCount != 0){
            anotherThread[k] = new SumThread(k * threadLength, array.length - 1, k);
        }

        for (k = 0; k < threadsCount; k++){
            anotherThread[k].start();
        }

        try {
            for (k = 0; k < threadsCount; k++){
                anotherThread[k].join();
            }
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }

        int byThreadSum = 0;

        for ( k = 0; k < threadsCount; k++) {
            byThreadSum += sums[k];
        }

        System.out.println("Сумма вычисленная в " + threadsCount + " потоках - " + byThreadSum);
    }

}



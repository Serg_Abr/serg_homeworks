create table product(
                        id serial primary key,
                        title varchar(30),
                        price money,
                        quantity integer check (quantity > 0)
);

insert into product(title, price, quantity) VALUES ('молоко', 30.2, 50);
insert into product(title, price, quantity) VALUES ('кефир', 40.4, 40);
insert into product(title, price, quantity) VALUES ('сметана', 96.5, 20);
insert into product(title, price, quantity) VALUES ('йогурт', 45.7, 30);

create table buyer(
                      id serial primary key,
                      name_buyer varchar(30)
);

insert into buyer(name_buyer) VALUES ('Иванов');
insert into buyer(name_buyer) VALUES ('Иванова');
insert into buyer(name_buyer) VALUES ('Петров');
insert into buyer(name_buyer) VALUES ('Сидоров');

create table orders(
                       id serial primary key,
                       id_product integer,
                       id_buyer integer,
                       data_order date,
                       count_order integer,
                       foreign key(id_product) references product(id),
                       foreign key(id_buyer) references buyer(id)
);

insert into  orders(id_product, id_buyer, data_order, count_order) VALUES (1,1,'2021-11-25',1);
insert into  orders(id_product, id_buyer, data_order, count_order) VALUES (1,2,'2021-11-26',2);
insert into  orders(id_product, id_buyer, data_order, count_order) VALUES (4,2,'2021-11-26',2);
insert into  orders(id_product, id_buyer, data_order, count_order) VALUES (3,2,'2021-11-25',1);
insert into  orders(id_product, id_buyer, data_order, count_order) VALUES (1,4,'2021-11-27',1);
insert into  orders(id_product, id_buyer, data_order, count_order) VALUES (4,4,'2021-11-25',3);

-- вывод названия продуктов
SELECT title FROM product;

-- список продуктов с указанием общей стоимости с сортировкой по цене
SELECT title, price, quantity, quantity * price AS sum
FROM product
ORDER BY price;

-- продукты купленные 26 числа и их количество
SELECT title, count_order
FROM product
         JOIN orders o ON product.id = o.id_product
WHERE data_order = '2021-11-26';

-- список покупателей с их покупками и стоимостью покупок
SELECT buyer.name_buyer,
       product.title,
       orders.data_order,
       orders.count_order * product.price as totalSum
FROM buyer,
     product,
     orders
WHERE orders.id_product = product.id
  AND orders.id_buyer = buyer.id;

-- изменение цены на продукт и его количества
UPDATE product
SET price = 31.0, quantity = 20
WHERE id = 1;
import java.util.List;

public class Main {

    public static void printArr (List<User> arr) {
        if (arr.size() == 0) {
            System.out.println("Список пустой");
        } else {
            for (User user : arr) {
                System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
            }
        }
    }
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();
        printArr(users);

        System.out.println();
        List<User> usersAge = usersRepository.findByAge(25);
        printArr(usersAge);

        System.out.println();
        List<User> usersWork = usersRepository.findByIsWorkerIsTrue();
        printArr(usersWork);

    }
}
